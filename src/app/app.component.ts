import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { convertLocalDateTimeToUTCISO, convertLocalDateToUTCISO } from './util/date.util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  
  data = this.fb.group({
    dateUTC : [''],
    dateLocal : [''],
    dateTimeUTC : [''],
    dateTimeLocal : [''],
  })

  constructor(private fb : FormBuilder) {}

  ngOnInit(): void {
    this.data.get('dateLocal')?.valueChanges.subscribe(result => {
      this.data.patchValue({
        dateUTC : convertLocalDateToUTCISO(result)
      })
    })
    
    this.data.get('dateTimeLocal')?.valueChanges.subscribe(result => {
      this.data.patchValue({
        dateTimeUTC : convertLocalDateTimeToUTCISO(result)
      })
    })
  }
}